<?php
/**
 * @file
 * pab_deposit.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function pab_deposit_default_rules_configuration() {
  $items = array();
  $items['rules_deposit_pricing'] = entity_import('rules_config', '{ "rules_deposit_pricing" : {
    "LABEL" : "Deposit Pricing",
    "PLUGIN" : "reaction rule",
    "WEIGHT" : "-1",
    "OWNER" : "rules",
    "REQUIRES" : [ "pab_deposit", "rules" ],
    "ON" : { "pab_deposit_apply" : [] },
    "IF" : [
      { "payment_is_deposit" : { "order" : [ "commerce-order" ] } },
      { "data_is" : {
          "data" : [ "commerce-order:commerce-order-total:amount" ],
          "op" : "\u003E",
          "value" : "2499"
        }
      }
    ],
    "DO" : [
      { "pab_deposit_price" : { "commerce_order" : [ "commerce_order" ], "amount" : "2500" } }
    ]
  }
}');
  $items['rules_set_to_original_price'] = entity_import('rules_config', '{ "rules_set_to_original_price" : {
      "LABEL" : "Set to original price",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "pab_deposit", "commerce_checkout" ],
      "ON" : { "commerce_checkout_complete" : [] },
      "IF" : [ { "payment_is_deposit" : { "order" : [ "commerce-order" ] } } ],
      "DO" : [ { "pab_booked_price" : { "commerce_order" : [ "commerce_order" ] } } ]
    }
  }');
  return $items;
}
