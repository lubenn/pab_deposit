<?php

/**
 * Implements hook_rules_action_info().
 */
function pab_deposit_rules_action_info() {
  $actions = array(
    'pab_deposit_price' => array(
      'label' => t('Set the deposit pricing (fixed amount)'),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order'),
        ),
        'amount' => array(
          'type' => 'decimal',
          'label' => t('Amount'),
          'description' => 'Deposit amount.',
        ),
      ),
      'group' => t('Commerce Payment'),
    ),
    'pab_booked_price' => array(
      'label' => t('Set to original pricing'),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order'),
        ),
      ),
      'group' => t('Commerce Payment'),
    ),
  );
  return $actions;
}

function pab_deposit_rules_condition_info() {
  return array(
    'payment_is_deposit' => array(
      'label' => t('Paying deposit'),
      'parameter' => array(
        'order' => array(
          'label' => t('Commerce Order'),
          'type' => 'commerce_order',
        ),
      ),
      'group' => t('Commerce Payment'),
    ),
  );
}

function pab_deposit_price($order, $amount) {
  if($order->field_full_amount[LANGUAGE_NONE][0]['amount'] <= $amount) {
    return;
  }

  $difference = 0;

  if($order->field_full_amount[LANGUAGE_NONE][0]['amount'] > $amount) {
    $difference = $order->field_full_amount[LANGUAGE_NONE][0]['amount'] - $amount;
  }

  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  if($difference) {
    $discount_amount = array(
       'amount' => -$difference,
       'currency_code' => 'GBP',
       'data' => array(
        'components' => array()
       )
    );

    if (!pab_deposit_set_existing_line_item_price($order_wrapper, 'pab_deposit', $discount_amount)) {
      pab_deposit_add_line_item($order_wrapper, 'pab_deposit', $discount_amount);
    }

    // Update the total order price, for the next rules condition (if any).
    pab_deposit_calculate_order_total($order_wrapper);
  }
}

function pab_deposit_set_existing_line_item_price(EntityDrupalWrapper $order_wrapper, $discount_name, $discount_price) {
  $modified_line_item = FALSE;

  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    if ($line_item_wrapper->getBundle() == 'pab_deposit') {
      // Add the discount component price if the line item was originally
      $line_item = $line_item_wrapper->value();
      if (isset($line_item->data['discount_name']) && $line_item->data['discount_name'] == $discount_name) {
        pab_deposit_set_price_component($line_item_wrapper, $discount_name, $discount_price);
        $line_item_wrapper->save();
        $modified_line_item = $line_item;
        break;
      }
    }
  }

  return $modified_line_item;
}

function pab_deposit_add_line_item(EntityDrupalWrapper $order_wrapper, $discount_name, $discount_amount, $data = array()) {
  // Create a new line item.
  $discount_line_item = commerce_line_item_new('pab_deposit', $order_wrapper->getIdentifier());
  $discount_line_item->data = array('discount_name' => $discount_name) + $data;

  // Setting the bundle to ensure the entity metadata stores it correctly.
  $info = array('bundle' => 'pab_deposit');
  $discount_line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $discount_line_item, $info);

  // Initialize the line item unit price.
  $discount_line_item_wrapper->commerce_unit_price->amount = 0;
  $discount_line_item_wrapper->commerce_unit_price->currency_code = $discount_amount['currency_code'];

  // Reset the data array of the line item total field to only include a base
  // price component and set the currency code from the order.
  $base_price = array(
    'amount' => 0,
    'currency_code' => $discount_amount['currency_code'],
    'data' => array(),
  );
  $discount_line_item_wrapper->commerce_unit_price->data = commerce_price_component_add($base_price, 'base_price', $base_price, TRUE);

  // Add the discount price component.
  pab_deposit_add_price_component($discount_line_item_wrapper, $discount_name, $discount_amount);

  // Save the incoming line item now so we get its ID and add it to the oder's
  // line item reference field value.
  commerce_line_item_save($discount_line_item);
  $order_wrapper->commerce_line_items[] = $discount_line_item;

  return $discount_line_item;
}

function pab_deposit_set_price_component(EntityDrupalWrapper $line_item_wrapper, $discount_name, $discount_amount) {
  $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);
  // Currencies don't match, abort.
  if ($discount_amount['currency_code'] != $unit_price['currency_code']) {
    return;
  }

  $discount_amount['data'] = array(
    'discount_name' => $discount_name,
    'discount_component_title' => t('Deposit'),
  );

  // Set the new unit price.
  $line_item_wrapper->commerce_unit_price->amount = $discount_amount['amount'];

  // Add the discount amount as a price component.
  $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);

  $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add($unit_price, 'deposit', $discount_amount, TRUE, TRUE);
}

function pab_deposit_add_price_component(EntityDrupalWrapper $line_item_wrapper, $discount_name, $discount_amount) {
  $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);
  $current_amount = $unit_price['amount'];
  // Currencies don't match, abort.
  if ($discount_amount['currency_code'] != $unit_price['currency_code']) {
    return;
  }

  // Calculate the updated amount and create a price array representing the
  // difference between it and the current amount.
  $discount_amount['amount'] = commerce_round(COMMERCE_ROUND_HALF_UP, $discount_amount['amount']);

  $updated_amount = $current_amount + $discount_amount['amount'];

  $difference = array(
    'amount' => $discount_amount['amount'],
    'currency_code' => $discount_amount['currency_code'],
    'data' => array(
      'discount_name' => $discount_name,
      'discount_component_title' => t('Deposit'),
    ),
  );

  // Set the new unit price.
  $line_item_wrapper->commerce_unit_price->amount = $updated_amount;

  // Add the discount amount as a price component.
  $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);
  $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add($unit_price, 'deposit', $difference, TRUE, TRUE);
}

/**
 * Rules action: set the line item price to the booked price.
 */
function pab_booked_price($order) {
  if(isset($order->data['payment_type'])) {
    unset($order->data['payment_type']);
    commerce_order_save($order);
    commerce_cart_order_refresh($order);
  }
}

function payment_is_deposit($order) {
  if(!empty($order->data) && isset($order->data['payment_type']) && $order->data['payment_type'] == 'deposit') {
    return TRUE;
  }
  return FALSE;
}
